<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class IntellipharmAPIController extends Controller
{

    public function getChartData($year = null)
    {

        if($year !== null && $year > 1980 && $year < 2100) {
            $chartData = DB::select(
                "select count(id) as member_count, 
                MONTHNAME(joined_date) as time_joined,
                MONTH(joined_date) as mth_int
                from members 
                where YEAR(joined_date) = $year 
                group by time_joined, mth_int
                order by mth_int asc"
            );

        } else {
            $chartData = DB::select(
                "select count(id) as member_count, 
                YEAR(joined_date) as time_joined 
                from members 
                group by YEAR(joined_date)"
            );
        }
        return response()->json(['labels' => array_column($chartData, 'time_joined'), 'data' => array_column($chartData, 'member_count')]);
    }

    public function getHtmlTableData() {

        $firstname = Input::get('firstname', null);
        $lastname = Input::get('lastname', null);
        $email = Input::get('email', null);
        $page = Input::get('page', 1);

        $memberQuery = Member::query();

        if($firstname !== null) {
            $memberQuery->where('firstname', 'LIKE', '%' . $firstname . '%');
        }

        if($lastname !== null) {
            $memberQuery->where('surname', 'LIKE', '%' . $lastname . '%');
        }

        if($email !== null) {
            $memberQuery->where('email', 'LIKE', '%' . $email . '%');
        }

        $offset = (intval($page) - 1) * 10;
        $limit = 10;

        $maxPages = ceil($memberQuery->count() / 10);
        $memberQuery->offset($offset)->limit($limit);

        return response()->json(['data' => $memberQuery->get(), 'maxPages' => $maxPages]);

    }

    public function postMembers()
    {

        // Get all the table inputs

        $columns = Input::get('columns', null);
        $order = Input::get('order', null);
        $start = Input::get('start', 0);
        $length = Input::get('length', 10);
        $search = Input::get('search', null);

        // Get an empty query builder for members
        $memberQuery = Member::query();

        // Searching logic

        if($columns !== null && $search !== null) {

            $searchTerm = $search['value'];
            if ($searchTerm !== null) {
                foreach ($columns as $column) {

                    $columnName = $column['data'] ?? null;
                    if($columnName !== null) {
                        $memberQuery->orWhere($columnName, 'LIKE', '%' . $searchTerm . '%');
                    }
                }
            }
        }

        // Sorting logic
        if($order !== null && is_array($order)) {

            foreach($order as $orderDirective) {
                $columnName = $columns[$orderDirective['column']]['data'] ?? null;
                $direction = $orderDirective['dir'] ?? null;

                if($columnName !== null && $direction !== null) {
                    $memberQuery->orderBy($columnName, $direction);
                }
            }

        }

        // Pagination

        $allRecordQty = Member::all()->count();
        $payloadQty = $memberQuery->count();

        $memberQuery->offset($start)->limit($length);

        $payload = $memberQuery->get();

        return response()->json(
            [
                'data' => $payload,
                'recordsTotal' => $allRecordQty,
                'recordsFiltered' => $payloadQty
            ]
        );

    }
}
