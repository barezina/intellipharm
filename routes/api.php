<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/intellipharm-members', 'IntellipharmAPIController@postMembers');
Route::get('/intellipharm-chartdata/{year?}', 'IntellipharmAPIController@getChartData');
Route::get('/intellipharm-htmltabledata', 'IntellipharmAPIController@getHtmlTableData');

