<script>
    $(document).ready(function() {
        $('#example').DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "api/intellipharm-members",
                "type": "POST"
            },
            "columns": [
                { "data": "firstname" },
                { "data": "surname" },
                { "data": "email" },
                { "data": "gender" },
                { "data": "joined_date" }
            ]
        } );
    } );
</script>

<table id="example" class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
        <th>Gender</th>
        <th>Join Date</th>
    </tr>
    </thead>
</table>

