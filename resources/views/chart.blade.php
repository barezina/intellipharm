<div class="chart-container">

    <canvas id="intellipharm-chart"></canvas>

    <button class="btn btn-default reset-chart">
        All Years
    </button>

</div>

<script>

    var intellipharmChart;

    function generateChartWithYear(year) {

        var url;
        var legendTitle;

        if(year == null) {
            url = '/api/intellipharm-chartdata';
            legendTitle = 'Year Joined';
            $('.reset-chart').hide();

        } else {
            url = '/api/intellipharm-chartdata/' + year;
            legendTitle = 'Members Joined in ' + year;
            $('.reset-chart').show();
        }

        $.ajax({
            url: url

        }).done(function( response ) {

            chartData = response.data;
            chartLabels = response.labels;

            var options = {
                type: 'line',
                data: {
                    labels: chartLabels,
                    datasets: [
                        {
                            label: legendTitle,
                            data: chartData,
                            borderWidth: 1
                        }
                    ]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                stepSize: 1,
                                reverse: false
                            }
                        }]
                    }
                }
            }

            var ctx = $("#intellipharm-chart")[0].getContext('2d');

            if(typeof(intellipharmChart) !== 'undefined') {
                intellipharmChart.destroy();
            }

            intellipharmChart = new Chart(ctx, options);

        });

    }

    $(document).ready(function() {

        var yearExamined = null;
        intellipharmChart = generateChartWithYear(yearExamined);

        $('#intellipharm-chart').on('click', function(evt) {

            if(yearExamined !== null) {
                return;
            }

            var item = intellipharmChart.getElementAtEvent(evt)[0];
            yearExamined = intellipharmChart.data.labels[item._index];

            generateChartWithYear(yearExamined);

        });

        $('.reset-chart').on('click', function() {

            yearExamined = null;
            generateChartWithYear(yearExamined);

        });
    });

</script>