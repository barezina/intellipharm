@extends('i-base')
@section('content')


    <div class="i-chart">
        <h4>Chart that allows clicking to show monthly data</h4>
        <p>Click on any year's data-point to show that year in detail.</p>

        @include('chart')

    </div>

    <div class="i-htmltable">
        <h4>Plain HTML table with three field searching and pagination</h4>
        <p>Search boxes query the backend 250ms after keyup.</p>

        @include('htmltable')

    </div>

    <div class="i-table">
        <h4>Third party table with serverside sorting, searching and pagination</h4>
        <p>Search box performs search on all columns.</p>

        @include('table')

    </div>

@endsection