
<div class="html-table-container">

    <div class="html-table-input-container">
        <input type="text" class="form-control col-md-4 html-table-inputs firstname-input" placeholder="First Name">
        <input type="text" class="form-control col-md-4 html-table-inputs lastname-input" placeholder="Last Name">
        <input type="text" class="form-control col-md-4 html-table-inputs email-input" placeholder="Email Address">

    </div>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Email</th>
            <th scope="col">Gender</th>
            <th scope="col">Join Date</th>
        </tr>
        </thead>
        <tbody class="html-table-dynamic">
        </tbody>
    </table>

    <div class="button-container pull-right col-md-12">
        <button type="button" class="btn btn-default prev-button">Previous</button>
        <button type="button" class="btn btn-default next-button">Next</button>
    </div>

</div>

<script>

    var page = 1;
    var timer;

    function generateNewTable() {

        tableBody = $('.html-table-dynamic');

        var payload = {};
        var firstname = $('.firstname-input').val();
        var lastname = $('.lastname-input').val();
        var email = $('.email-input').val();

        if(firstname !== '') {
            payload['firstname'] = firstname;
        }

        if(lastname !== '') {
            payload['lastname'] = lastname;
        }

        if(email !== '') {
            payload['email'] = email;
        }

        payload['page'] = page;

        $.ajax({
            url: '/api/intellipharm-htmltabledata',
            type: 'GET',
            data: payload

        }).done(function(response) {

            $('.html-table-dynamic').empty();

            if(page == 1) {
                $('.prev-button').attr('disabled', true);
            } else {
                $('.prev-button').attr('disabled', false);
            }

            if(page < response.maxPages) {
                $('.next-button').attr('disabled', false);
            } else {
                $('.next-button').attr('disabled', true);
            }

            for (var i = 0; i < response.data.length; i++) {
                record = response.data[i];

                $(tableBody).append('<tr>');
                $(tableBody).append('<td>' + record.firstname + '</td>');
                $(tableBody).append('<td>' + record.surname + '</td>');
                $(tableBody).append('<td>' + record.email + '</td>');
                $(tableBody).append('<td>' + record.gender + '</td>');
                $(tableBody).append('<td>' + record.joined_date + '</td>');
                $(tableBody).append('</tr>');
            }
        });

    }

    function triggerTableRefresh() {

        // Queue the table to refresh in 250ms from now
        // If a timer exists already, invalidate that one and start a new one.
        // This delay exists so we dont hammer the server every time a letter is pressed.

        if(typeof(timer) !== 'undefined') {
            clearTimeout(timer);
        }

        timer = setTimeout(function() {
            generateNewTable();
        }, 250);

    }


    $(document).ready(function() {
        $('.firstname-input').keyup(function() {
            page = 1;
            triggerTableRefresh();
        });

        $('.lastname-input').keyup(function() {
            page = 1;
            triggerTableRefresh();
        })

        $('.email-input').keyup(function() {
            page = 1;
            triggerTableRefresh();
        });

        $('.next-button').on('click', function() {
            page++;
            triggerTableRefresh();
        });

        $('.prev-button').on('click', function() {
            page--;
            triggerTableRefresh();
        });

        triggerTableRefresh();
    });

</script>